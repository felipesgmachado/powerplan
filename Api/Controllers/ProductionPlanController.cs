﻿using ApplicationCore.ProductionLoad;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Api.Controllers
{
    [Route("api/productionplan")]
    [ApiController]
    public class ProductionPlanController : ControllerBase
    {
        private readonly IAddProductionLoadUseCase _addProductionLoadUseCase;

        public ProductionPlanController(IAddProductionLoadUseCase addProductionLoadUseCase)
        {
            _addProductionLoadUseCase = addProductionLoadUseCase;
        }

        [HttpPost]        
        public IActionResult AddLoad([FromBody] AddProductionLoadInput input)
        {
            var result = _addProductionLoadUseCase.Handle(input);            

            if (result.IsError)
            {
                Log.Error("productionplan BadRequest error => {@result}", result);
                return BadRequest();
            }
            
            return Ok(result.ProductPlan);
        }
    }
}
