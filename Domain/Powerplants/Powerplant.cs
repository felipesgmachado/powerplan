﻿namespace Domain.Powerplants;

public class Powerplant
{
    public Powerplant(string name, string type, decimal efficiency, decimal pmin, decimal pmax)
    {
        Id = Guid.NewGuid();
        Name = name;

        if (type.ToLower() == Type.Windturbine.ToString().ToLower())
            Type = Type.Windturbine;
        if (type.ToLower() == Type.Gasfired.ToString().ToLower())
            Type = Type.Gasfired;
        if (type.ToLower() == Type.Turbojet.ToString().ToLower())
            Type = Type.Turbojet;
                               
        Efficiency = efficiency;
        PMin = pmin;
        PMax = pmax;
    }

    public Guid Id { get; private set; }
    public string Name { get; private set; }    
    public Type Type { get; private set; }
    public decimal Efficiency { get; private set; }
    public decimal PMin { get; private set; }
    public decimal PMax { get; private set; }
}

