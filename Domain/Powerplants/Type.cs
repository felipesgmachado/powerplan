﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Powerplants;

public enum Type
{
    Windturbine = 1,
    Gasfired = 2,
    Turbojet = 3
}
