﻿namespace Domain.Fuels;

public class Fuel
{
    public Fuel(decimal gas, decimal kerosine, int co2, int wind)
    {
        Id = Guid.NewGuid();
        Gas = gas;
        Kerosine = kerosine;
        Co2 = co2;
        Wind = wind;        
    }

    public Guid Id { get; private set; }
    public decimal Gas { get; private set; }
    public decimal Kerosine { get; private set; }
    public decimal Co2 { get; private set; }
    public decimal Wind { get; private set; }
}
