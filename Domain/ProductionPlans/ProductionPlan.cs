﻿using Domain.Powerplants;

namespace Domain.ProductionPlan;

public class ProductionPlan
{
    public ProductionPlan(Powerplant powerplant, decimal power)
    {
        Id = Guid.NewGuid();
        Powerplant = powerplant;
        Power = power;
    }
    public Guid Id { get; private set; }
    public Powerplant Powerplant { get; private set; }
    public decimal Power { get; private set; }
}
