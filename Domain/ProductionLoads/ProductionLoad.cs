﻿using Domain.Fuels;
using Domain.Powerplants;

namespace Domain.ProductionLoad;

public class ProductionLoad
{
    public ProductionLoad(int load, List<Fuel> fuels, List<Powerplant> powerplants)
    {
        Id = Guid.NewGuid();
        Load = load;
        Fuels = fuels;
        Powerplants = powerplants;  
    }

    public Guid Id { get; private set; }
    public int Load { get; private set; }
    public List<Fuel> Fuels { get; private set; }
    public List<Powerplant> Powerplants { get; private set; }
}
