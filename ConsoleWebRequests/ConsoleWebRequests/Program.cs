﻿using System;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace ConsoleWebRequests;

//Console App used to make a local request to the productionplan API
public class Program
{
    static async Task Main()
    {
        // endpoint URL 
        string apiUrl = "https://localhost:8888/api/productionplan";

        // JSON Path
        var jsonFilePath = new StringBuilder();
        string startupPath = Environment.CurrentDirectory;
        jsonFilePath.Append(startupPath);
        jsonFilePath.Append(@"\JsonFiles\payload3.json");
        

        // File validation
        if (File.Exists(jsonFilePath.ToString()))
        {
            // Read JSON file
            string jsonBody = File.ReadAllText(jsonFilePath.ToString());

            using (HttpClient client = new HttpClient())
            {
                // Setting head request
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                // Setting content request
                StringContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                // POST execution
                HttpResponseMessage response = await client.PostAsync(apiUrl, content);

                // Success validation
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Request successful!");
                    string responseData = await response.Content.ReadAsStringAsync();
                    Console.WriteLine($"Server response: {responseData}");
                }
                else
                {
                    Console.WriteLine($"Error in the request. Status code: {response.StatusCode}");
                }
            }
        }
        else
        {
            Console.WriteLine($"JSON file not found in {jsonFilePath}");
        }
    }
}
