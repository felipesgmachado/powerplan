﻿using Domain.Powerplants;
using Domain.ProductionPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Domain;

public class ProductionPlanTests
{
    [Fact]
    public void ProductionPlan_Constructor_ValidValues_ShouldCreateInstance()
    {
        // Arrange
        Powerplant powerplant = new Powerplant("gasfiredbig1", "Gasfired", 0.53M, 100, 460);
        decimal power = 200;

        // Act
        ProductionPlan productionPlan = new ProductionPlan(powerplant, power);

        // Assert
        Assert.NotNull(productionPlan);
        Assert.IsType<ProductionPlan>(productionPlan);
        Assert.NotEqual(Guid.Empty, productionPlan.Id);
        Assert.Equal(powerplant, productionPlan.Powerplant);
        Assert.Equal(power, productionPlan.Power);
    }    
}
