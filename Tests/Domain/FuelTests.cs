﻿using Domain.Fuels;
using System;
using Xunit;

namespace Tests.Domain;

public class FuelTests
{
    [Fact]
    public void Fuel_Constructor_ValidValues_ShouldCreateInstance()
    {
        // Arrange
        decimal gas = 13.4M;
        decimal kerosine = 50.8M;
        int co2 = 20;
        int wind = 60;

        // Act
        Fuel fuel = new Fuel(gas, kerosine, co2, wind);

        // Assert
        Assert.NotNull(fuel);
        Assert.IsType<Fuel>(fuel);
        Assert.NotEqual(Guid.Empty, fuel.Id);
        Assert.Equal(gas, fuel.Gas);
        Assert.Equal(kerosine, fuel.Kerosine);
        Assert.Equal(co2, fuel.Co2);
        Assert.Equal(wind, fuel.Wind);
    }    
}
