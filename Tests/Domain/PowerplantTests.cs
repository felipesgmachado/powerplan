﻿using Domain.Powerplants;
using Type = Domain.Powerplants.Type;
using System;
using Xunit;

namespace Tests.Domain;

public class PowerplantTests
{
    [Fact]
    public void Powerplant_Constructor_ValidValues_ShouldCreateInstance()
    {
        // Arrange
        string name = "gasfiredbig1";
        string type = "Gasfired";
        decimal efficiency = 0.53M;
        decimal pmin = 100;
        decimal pmax = 460;

        // Act
        Powerplant powerplant = new Powerplant(name, type, efficiency, pmin, pmax);

        // Assert
        Assert.NotNull(powerplant);
        Assert.IsType<Powerplant>(powerplant);
        Assert.NotEqual(Guid.Empty, powerplant.Id);
        Assert.Equal(name, powerplant.Name);
        Assert.Equal(Type.Gasfired, powerplant.Type);
        Assert.Equal(efficiency, powerplant.Efficiency);
        Assert.Equal(pmin, powerplant.PMin);
        Assert.Equal(pmax, powerplant.PMax);
    }

    [Fact]       
    public void Powerplant_Constructor_ValidWindTurbine_ShouldCreateInstance()
    {
        // Arrange
        string name = "windpark1";
        string type = "Windturbine";
        decimal efficiency = 1;
        decimal pmin = 0;
        decimal pmax = 150;

        // Act
        Powerplant powerplant = new Powerplant(name, type, efficiency, pmin, pmax);

        // Assert
        Assert.NotNull(powerplant);
        Assert.IsType<Powerplant>(powerplant);
        Assert.Equal(Type.Windturbine, powerplant.Type);
    }

    [Fact]    
    public void Powerplant_Constructor_ValidTurbojet_ShouldCreateInstance()
    {
        // Arrange
        string name = "tj1";
        string type = "Turbojet";
        decimal efficiency = 0.3m;
        decimal pmin = 0;
        decimal pmax = 16;

        // Act
        Powerplant powerplant = new Powerplant(name, type, efficiency, pmin, pmax);

        // Assert
        Assert.NotNull(powerplant);
        Assert.IsType<Powerplant>(powerplant);
        Assert.Equal(Type.Turbojet, powerplant.Type);
    }
}
