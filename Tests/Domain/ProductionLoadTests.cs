﻿using Domain.Fuels;
using Domain.Powerplants;
using Domain.ProductionLoad;
using System;
using System.Collections.Generic;
using Xunit;

namespace Tests.Domain;

public class ProductionLoadTests
{
    [Fact]
    public void ProductionLoad_Constructor_ValidValues_ShouldCreateInstance()
    {
        // Arrange
        int load = 910;
        List<Fuel> fuels = new List<Fuel>
        {
            new Fuel(13.4M, 50.8M, 20, 60),
        };
        List<Powerplant> powerplants = new List<Powerplant>
        {
            new Powerplant("gasfiredbig1", "Gasfired", 0.53M, 100, 460),
        };

        // Act
        ProductionLoad productionLoad = new ProductionLoad(load, fuels, powerplants);

        // Assert
        Assert.NotNull(productionLoad);
        Assert.IsType<ProductionLoad>(productionLoad);
        Assert.NotEqual(Guid.Empty, productionLoad.Id);
        Assert.Equal(load, productionLoad.Load);
        Assert.Equal(fuels, productionLoad.Fuels);
        Assert.Equal(powerplants, productionLoad.Powerplants);
    }    
}
