﻿using Api.Controllers;
using ApplicationCore.ProductionLoad;
using ApplicationCore.ProductionPlan;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http.Json;
using System.Text;

namespace Tests.Api;

public class ProductionPlanControllerTestsIntegration
{
    [Fact]
    public async Task AddLoad_ValidInput_ShouldReturnOkResult()
    {
        // Arrange
        var jsonFilePath = new StringBuilder();
        string startupPath = Environment.CurrentDirectory;
        jsonFilePath.Append(startupPath);
        jsonFilePath.Append(@"\JsonFiles\payload3.json");

        if (!File.Exists(jsonFilePath.ToString()))
        {
            throw new FileNotFoundException($"JSON File not found in {jsonFilePath}");
        }

        var application = new ProductionPlanWebApplicationFactory();

        var client  = application.CreateClient();

        // Act
        string jsonBody = File.ReadAllText(jsonFilePath.ToString());
        var response = await client.PostAsync("https://localhost:8888/api/productionplan", new StringContent(jsonBody, Encoding.UTF8, "application/json"));    

        // Assert
        response.EnsureSuccessStatusCode();
        response.Should().BeSuccessful();
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);              
    }
}
