﻿using ApplicationCore.ProductionPlan;
using DomainLayerProductionPlan = Domain.ProductionPlan;
using DomainLayerPowerplant = Domain.Powerplants;

namespace Tests.ApplicationCore;

public class GetProductionPlanOutputTests
{
    [Fact]
    public void Constructor_WithEntities_ShouldCreateInstance()
    {
        // Arrange
        var entities = new List<DomainLayerProductionPlan.ProductionPlan>
        {
            new DomainLayerProductionPlan.ProductionPlan(new DomainLayerPowerplant.Powerplant("gasfiredbig1", "Gasfired", 0.53M, 100, 460), 100),
            new DomainLayerProductionPlan.ProductionPlan(new DomainLayerPowerplant.Powerplant("windturbine1", "Windturbine", 1, 0, 150), 50)
        };

        // Act
        var output = new GetProductionPlanOutput(entities);

        // Assert
        Assert.NotNull(output);
        Assert.IsType<GetProductionPlanOutput>(output);
        Assert.False(output.IsError);
        Assert.Equal(2, output.ProductPlan.Count);
        Assert.Equal("gasfiredbig1", output.ProductPlan[0].Name);
        Assert.Equal(100, output.ProductPlan[0].P);
        Assert.Equal("windturbine1", output.ProductPlan[1].Name);
        Assert.Equal(50, output.ProductPlan[1].P);
    }

    [Fact]
    public void Constructor_WithEmptyEntities_ShouldCreateInstanceWithEmptyProductPlan()
    {
        // Arrange
        var entities = new List<DomainLayerProductionPlan.ProductionPlan>();

        // Act
        var output = new GetProductionPlanOutput(entities);

        // Assert
        Assert.NotNull(output);
        Assert.IsType<GetProductionPlanOutput>(output);
        Assert.False(output.IsError);
        Assert.Empty(output.ProductPlan);
    }
}
