﻿using ApplicationCore.ProductionLoad;
using ApplicationCore.ProductionPlan;
using Domain.Powerplants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.ApplicationCore;

public class AddProductionLoadUseCaseTests
{
    [Fact]
    public void CalculatePowerForPowerplant_Windturbine_ShouldCalculatePower()
    {
        // Arrange
        var windturbine = new Powerplant("windturbine1", "Windturbine", 1, 0, 150);
        decimal fuel = 50;
        decimal load = 100;

        // Act
        decimal power = new AddProductionLoadUseCase().CalculatePowerForPowerplant(windturbine, fuel, load);

        // Assert
        Assert.Equal(75, power);
    }

    [Fact]
    public void CalculatePowerForPowerplant_Gasfired_ShouldCalculatePower()
    {
        // Arrange
        var gasfired = new Powerplant("gasfired1", "Gasfired", 0.5M, 50, 200);
        decimal fuel = 30;
        decimal load = 100;

        // Act
        decimal power = new AddProductionLoadUseCase().CalculatePowerForPowerplant(gasfired, fuel, load);

        // Assert
        Assert.Equal(100, power);
    }

    [Fact]
    public void CalculatePowerForPowerplant_Turbojet_ShouldCalculatePower()
    {
        // Arrange
        var turbojet = new Powerplant("turbojet1", "Turbojet", 0.3M, 0, 16);
        decimal fuel = 10;
        decimal load = 8;

        // Act
        decimal power = new AddProductionLoadUseCase().CalculatePowerForPowerplant(turbojet, fuel, load);

        // Assert
        Assert.Equal(8, power);
    }

    [Fact]
    public void CalculatePower_ShouldCalculatePowerForAllPowerplants()
    {
        // Arrange
        var windturbine = new Powerplant("windturbine1", "Windturbine", 1, 0, 150);
        var gasfired = new Powerplant("gasfired1", "Gasfired", 0.5M, 50, 200);
        var turbojet = new Powerplant("turbojet1", "Turbojet", 0.3M, 0, 16);

        var powerplants = new List<Powerplant> { windturbine, gasfired, turbojet };

        decimal fuel = 100;
        decimal load = 200;

        // Act
        var productionPlans = new AddProductionLoadUseCase().CalculatePower(powerplants, fuel, load);

        // Assert
        Assert.Equal(3, productionPlans.Count);
        Assert.Equal(150, productionPlans[0].Power);
        Assert.Equal(50, productionPlans[1].Power);
        Assert.Equal(0, productionPlans[2].Power);
    }

    [Fact]
    public void Handle_ValidInput_ShouldReturnGetProductionPlanOutput()
    {
        // Arrange
        var input = new AddProductionLoadInput
        {
            Load = 100,
            Fuels = new AddFuelInput
            {
                Gas = 10,
                Kerosine = 20,
                Co2 = 30,
                Wind = 40
            },
            Powerplants = new List<AddPowerplantInput>
            {
                new AddPowerplantInput { Name = "windturbine1", Type = "Windturbine", Efficiency = 1, PMin = 0, PMax = 150 },
                new AddPowerplantInput { Name = "gasfired1", Type = "Gasfired", Efficiency = 0.5M, PMin = 50, PMax = 200 },
                new AddPowerplantInput { Name = "turbojet1", Type = "Turbojet", Efficiency = 0.3M, PMin = 0, PMax = 16 }
            }
        };

        var useCase = new AddProductionLoadUseCase();

        // Act
        var result = useCase.Handle(input);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<GetProductionPlanOutput>(result);        
    }

    [Fact]
    public void CalculateProductionPlan_Windturbine_ShouldCalculateProductionPlan()
    {
        // Arrange
        var useCase = new AddProductionLoadUseCase();
        var windturbine = new Powerplant("windturbine1", "Windturbine", 1, 0, 150);
        var powerplants = new List<Powerplant> { windturbine };
        var fuels = new AddFuelInput { Wind = 50 };
        decimal load = 100;

        // Act
        var result = useCase.CalculateProductionPlan(powerplants, fuels, load);

        // Assert
        Assert.NotNull(result);
        Assert.Single(result);
        Assert.Equal("windturbine1", result[0].Powerplant.Name);
        Assert.Equal(75, result[0].Power);
    }

    [Fact]
    public void CalculateProductionPlan_Gasfired_ShouldCalculateProductionPlan()
    {
        // Arrange
        var useCase = new AddProductionLoadUseCase();
        var gasfired = new Powerplant("gasfired1", "Gasfired", 0.5M, 50, 200);
        var powerplants = new List<Powerplant> { gasfired };
        var fuels = new AddFuelInput { Gas = 30 };
        decimal load = 100;

        // Act
        var result = useCase.CalculateProductionPlan(powerplants, fuels, load);

        // Assert
        Assert.NotNull(result);
        Assert.Single(result);
        Assert.Equal("gasfired1", result[0].Powerplant.Name);
        Assert.Equal(100, result[0].Power);
    }

    [Fact]
    public void CalculateProductionPlan_Turbojet_ShouldCalculateProductionPlan()
    {
        // Arrange
        var useCase = new AddProductionLoadUseCase();
        var turbojet = new Powerplant("turbojet1", "Turbojet", 0.3M, 0, 16);
        var powerplants = new List<Powerplant> { turbojet };
        var fuels = new AddFuelInput { Kerosine = 10 };
        decimal load = 8;

        // Act
        var result = useCase.CalculateProductionPlan(powerplants, fuels, load);

        // Assert
        Assert.NotNull(result);
        Assert.Single(result);
        Assert.Equal("turbojet1", result[0].Powerplant.Name);
        Assert.Equal(8, result[0].Power);
    }
}
