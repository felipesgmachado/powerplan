﻿using ApplicationCore.ProductionLoad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Tests.ApplicationCore;

public class AddProductionLoadInputTests
{
    [Fact]
    public void AddProductionLoadInput_Constructor_ValidValues_ShouldCreateInstance()
    {
        // Arrange
        int load = 910;
        AddFuelInput fuels = new AddFuelInput
        {
            Gas = 13.4M,
            Kerosine = 50.8M,
            Co2 = 20,
            Wind = 60
        };
        List<AddPowerplantInput> powerplants = new List<AddPowerplantInput>
        {
            new AddPowerplantInput
            {
                Name = "gasfiredbig1",
                Type = "Gasfired",
                Efficiency = 0.53M,
                PMin = 100,
                PMax = 460
            },
            new AddPowerplantInput
            {
                Name = "windpark1",
                Type = "Windturbine",
                Efficiency = 1,
                PMin = 0,
                PMax = 150
            }
        };

        // Act
        AddProductionLoadInput input = new AddProductionLoadInput
        {
            Load = load,
            Fuels = fuels,
            Powerplants = powerplants
        };

        // Assert
        Assert.NotNull(input);
        Assert.Equal(load, input.Load);
        Assert.NotNull(input.Fuels);
        Assert.Equal(fuels.Gas, input.Fuels.Gas);
        Assert.Equal(fuels.Kerosine, input.Fuels.Kerosine);
        Assert.Equal(fuels.Co2, input.Fuels.Co2);
        Assert.Equal(fuels.Wind, input.Fuels.Wind);
        Assert.NotNull(input.Powerplants);
        Assert.Equal(powerplants.Count, input.Powerplants.Count);
        Assert.Equal(powerplants[0].Name, input.Powerplants[0].Name);
        Assert.Equal(powerplants[1].Name, input.Powerplants[1].Name);
    }

    [Fact]
    public void AddFuelInput_JsonPropertyName_ShouldHaveCorrectMapping()
    {
        // Arrange
        AddFuelInput fuels = new AddFuelInput
        {
            Gas = 13.4M,
            Kerosine = 50.8M,
            Co2 = 20,
            Wind = 60
        };

        // Act
        string jsonString = JsonSerializer.Serialize(fuels);

        // Assert
        Assert.Contains("\"gas(euro/MWh)\":13.4", jsonString);
        Assert.Contains("\"kerosine(euro/MWh)\":50.8", jsonString);
        Assert.Contains("\"co2(euro/ton)\":20", jsonString);
        Assert.Contains("\"wind(%)\":60", jsonString);
    }
}
