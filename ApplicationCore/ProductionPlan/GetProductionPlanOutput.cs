﻿using ApplicationCore.Shared;

namespace ApplicationCore.ProductionPlan;

public class GetProductionPlanOutput : BaseOutput
{
    public GetProductionPlanOutput()
    { }

    public GetProductionPlanOutput(List<Domain.ProductionPlan.ProductionPlan> entities)
    {
        IsError = false;

        ProductPlan = new List<ProductPlanOutput>();

        var list = entities.Select(productPlan => 
        new ProductPlanOutput { Name = entities.First().Powerplant.Name, P = entities.First().Power }).ToList();

        foreach (var entity in entities)
        {
            var productPlan = new ProductPlanOutput()
            {
                Name = entity.Powerplant.Name,
                P = entity.Power
            };  
            
            ProductPlan.Add(productPlan);
        }
    }

    public List<ProductPlanOutput> ProductPlan { get; set; }
}

public class ProductPlanOutput
{
    public string Name { get; set; }
    public decimal P { get; set; }
}
