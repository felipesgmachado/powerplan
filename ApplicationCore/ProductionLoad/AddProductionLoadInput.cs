﻿using ApplicationCore.Shared;
using System.Text.Json.Serialization;

namespace ApplicationCore.ProductionLoad;

public class AddProductionLoadInput : BaseInput
{
    public int Load { get; set; }
    public AddFuelInput Fuels { get; set; }
    public List<AddPowerplantInput> Powerplants { get; set; }
}

public class AddFuelInput
{
    [JsonPropertyName("gas(euro/MWh)")]
    public decimal Gas { get; set; }
    [JsonPropertyName("kerosine(euro/MWh)")]
    public decimal Kerosine { get; set; }
    [JsonPropertyName("co2(euro/ton)")]
    public decimal Co2 { get; set; }
    [JsonPropertyName("wind(%)")]
    public decimal Wind { get; set; }
}

public class AddPowerplantInput
{
    public string Name { get; set; }
    public string Type { get; set; }
    public decimal Efficiency { get; set; }
    public decimal PMin { get; set; }
    public decimal PMax { get; set; }
}
