﻿using FluentValidation;

namespace ApplicationCore.ProductionLoad;

public class AddProductionLoadInputValidation : AbstractValidator<AddProductionLoadInput>
{
    public AddProductionLoadInputValidation()
    {
        RuleFor(c => c.Load).NotNull().WithMessage("Load is required");
        RuleFor(c => c.Fuels).NotNull().WithMessage("Fuels is required");
        RuleFor(c => c.Powerplants).NotNull().WithMessage("Powerplant is required");        
    }
}
