﻿using ApplicationCore.ProductionPlan;
using ApplicationCore.Shared;
using Domain.Powerplants;
using Type = Domain.Powerplants.Type;

namespace ApplicationCore.ProductionLoad;

public class AddProductionLoadUseCase : BaseUseCase, IAddProductionLoadUseCase
{
    public GetProductionPlanOutput Handle(AddProductionLoadInput input)
    {
        if (!IsValid(new AddProductionLoadInputValidation(), input))
            return new GetProductionPlanOutput();

        if (input.Load == 0)
            return new GetProductionPlanOutput();

        if (input.Fuels is null)
            return new GetProductionPlanOutput();

        if (!input.Powerplants.Any())
            return new GetProductionPlanOutput();

        decimal load = (decimal)input.Load;

        var powerplantList = new List<Powerplant>();

        foreach (var item in input.Powerplants)
        {
            var powerplant = new Powerplant(item.Name, item.Type, item.Efficiency, item.PMin, item.PMax);
            powerplantList.Add(powerplant);
        }

        var productionPlanlist = CalculateProductionPlan(powerplantList, input.Fuels, load);

        return new GetProductionPlanOutput(productionPlanlist);
    }

    public List<Domain.ProductionPlan.ProductionPlan> CalculateProductionPlan(List<Powerplant> powerplants, AddFuelInput fuels, decimal load)
    {
        var productionPlanlist = new List<Domain.ProductionPlan.ProductionPlan>();

        var powerplantsMeritOrder1 = powerplants.Where(x => x.Type == Type.Windturbine).ToList();
        var productionPlanOrder1 = CalculatePower(powerplantsMeritOrder1, fuels.Wind, load);
        productionPlanlist.AddRange(productionPlanOrder1);

        var powerplantsMeritOrder2 = powerplants.Where(x => x.Type == Type.Gasfired).ToList();
        var productionPlanOrder2 = CalculatePower(powerplantsMeritOrder2, fuels.Gas, load);
        productionPlanlist.AddRange(productionPlanOrder2);

        var powerplantsMeritOrder3 = powerplants.Where(x => x.Type == Type.Turbojet).ToList();
        var productionPlanOrder3 = CalculatePower(powerplantsMeritOrder3, fuels.Kerosine, load);
        productionPlanlist.AddRange(productionPlanOrder3);

        return productionPlanlist;
    }    

    public List<Domain.ProductionPlan.ProductionPlan> CalculatePower(List<Powerplant> powerplantInputs, decimal fuel, decimal load)
    {
        var list = new List<Domain.ProductionPlan.ProductionPlan>();

        foreach (var powerplant in powerplantInputs)
        {
            decimal power = CalculatePowerForPowerplant(powerplant, fuel, load);

            var productionPlan = new Domain.ProductionPlan.ProductionPlan(powerplant, power);

            load -= power;

            list.Add(productionPlan);
        }

        return list;
    }

    public decimal CalculatePowerForPowerplant(Powerplant powerplant, decimal fuel, decimal load)
    {
        decimal power = 0;

        if (powerplant.Type == Type.Windturbine)
        {
            power = fuel * powerplant.PMax / 100;

            if (power >= load)
                power = load;
        }

        if (powerplant.Type == Type.Gasfired || powerplant.Type == Type.Turbojet)
        {
            power = powerplant.PMax;

            if (power >= load)
                power = load;
        }

        return power;
    }
}
