﻿using ApplicationCore.ProductionPlan;

namespace ApplicationCore.ProductionLoad;

public interface IAddProductionLoadUseCase
{
    //AT THE MOMENT IS NOT NECESSARY CREATE A ASYNC METHOD AND RETURN A TASK
    GetProductionPlanOutput Handle(AddProductionLoadInput input);
}
