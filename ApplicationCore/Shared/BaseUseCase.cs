﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Shared;

public class BaseUseCase
{
    protected bool IsValid<TV, TE>(TV validation, TE entity) where TV : AbstractValidator<TE> where TE : BaseInput
    {
        ValidationResult validator = validation.Validate(entity);

        return validator.IsValid;
    }
}
