## Introduction

In this project, a RESTful API called powerplan was developed in .NET 8 using aproaches as a clean architecture, Domain Driven Design and SOLID principles.

In short the idea of this project is Calculate how much power each of a multitude of different powerplants need to produce (a.k.a. the production-plan) when the load is given and taking into account the cost of the underlying energy sources (gas, kerosine) and the Pmin and Pmax of each powerplant.

## More in detail

The load is the continuous demand of power. The total load at each moment in time is forecasted. For instance for Belgium you can see the load forecasted by the grid operator here.

At any moment in time, all available powerplants need to generate the power to exactly match the load. The cost of generating power can be different for every powerplant and is dependent on external factors: The cost of producing power using a turbojet, that runs on kerosine, is higher compared to the cost of generating power using a gas-fired powerplant because of gas being cheaper compared to kerosine and because of the thermal efficiency of a gas-fired powerplant being around 50% (2 units of gas will generate 1 unit of electricity) while that of a turbojet is only around 30%. The cost of generating power using windmills however is zero. Thus deciding which powerplants to activate is dependent on the merit-order.

When deciding which powerplants in the merit-order to activate (a.k.a. unit-commitment problem) the maximum amount of power each powerplant can produce (Pmax) obviously needs to be taken into account. Additionally gas-fired powerplants generate a certain minimum amount of power when switched on, called the Pmin.

## Performing the challenge

Build a REST API exposing an endpoint /productionplan that accepts a POST of which the body contains a payload as you can find in the example_payloads directory and that returns a json with the same structure as in example_response.json and that manages and logs run-time errors.

For calculating the unit-commitment, we prefer you not to rely on an existing (linear-programming) solver.

The payload contains 3 types of data:

load: The load is the amount of energy (MWh) that need to be generated during one hour.
fuels: based on the cost of the fuels of each powerplant, the merit-order can be determined which is the starting point for deciding which powerplants should be switched on and how much power they will deliver. Wind-turbine are either switched-on, and in that case generate a certain amount of energy depending on the % of wind, or can be switched off.
gas(euro/MWh): the price of gas per MWh. Thus if gas is at 6 euro/MWh and if the efficiency of the powerplant is 50% (i.e. 2 units of gas will generate one unit of electricity), the cost of generating 1 MWh is 12 euro.
kerosine(euro/Mwh): the price of kerosine per MWh.
co2(euro/ton): the price of emission allowances (optionally to be taken into account).
wind(%): percentage of wind. Example: if there is on average 25% wind during an hour, a wind-turbine with a Pmax of 4 MW will generate 1MWh of energy.
powerplants: describes the powerplants at disposal to generate the demanded load. For each powerplant is specified:
name:
type: gasfired, turbojet or windturbine.
efficiency: the efficiency at which they convert a MWh of fuel into a MWh of electrical energy. Wind-turbines do not consume 'fuel' and thus are considered to generate power at zero price.
pmax: the maximum amount of power the powerplant can generate.
pmin: the minimum amount of power the powerplant generates when switched on.

# Build and launch the API

## Prerequisites:

Install .NET 8 SDK:
Ensure that you have the .NET 8 SDK installed on your machine. You can download it from the official Microsoft website: [Download .NET](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)

Instal Docker on your environment [Docker](https://www.docker.com/)

Integrated Development Environment (IDE):
Use an IDE of your choice, such as Visual Studio or Visual Studio Code.

## Execution Steps:

Implementations was submitted in C# (.Net 8) and contain a project file to compile the application.

1. Clone the Repository:
   Clone the project repository to your machine.

`git clone https://gitlab.com/felipesgmachado/powerplan.git`

2. Open the Project in the IDE:
   Open the project using your preferred IDE.

**Visual Studio Code**
: Go to the project solution folder and type
`code .`

**Visual Studio**:
Open Visual Studio and select "File" > "Open" > "Project/Solution," then choose the PowerPlan project's solution file (.sln).

3. Restore Dependencies:
   Open a terminal in the project folder and execute the following command to restore dependencies.
   `dotnet restore`

4. Run the Project:
   Execute the following command to start the application inside the API project folder: `dotnet run`.
   This will start the application and make the Web API available at a specific endpoint https://localhost:8888/api/productionplan

- There is a **DockerFile** inside the **API Project**. if you prefer:
- Use the follow command to Build the container: **docker build -t power-plan-api .**
- Use the follow command to RUN the API in a docker container: **docker run -p 8080:8080 power-plan-api**

5. Test the Web API:
   Open a browser or an API testing tool like Postman and test your Web API endpoints.

6. Stop the Application:
   To stop the application, press **Ctrl + C** in the terminal where the application is running.

### Links to endpoint

[Link to Swagger https endpoint](https://localhost:8888/swagger/index.html)

## Note about the project

I think maybe I made the project a little more complex than necessary. However, the reason for having created the project like this, with multiple layers in separate projects, is in addition to providing code decoupling and demonstrating a bit of Clean Architecture's separation of responsibilities. It is because I found this project very interesting and I intend to continue developing it. 
I will include more improvements and new features such as a database, more endpoints, and perhaps a frontend. Additionally, the inclusion of design patterns such as CQRS with Mediator, Unity of Working, and Repository. So that I can better apply the principles of SOLID.

## Technical Requirements
- [.NET 8 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)
- [Docker](https://www.docker.com/)
